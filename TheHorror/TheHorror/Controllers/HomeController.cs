﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TheHorror.Controllers
{
    public class HomeController : Controller
    {
        Random rnd = new Random();

        public IActionResult Index(int qty = 1)
        {
            ViewData["occurrences"] = qty;
            
            ViewBag.one = One()[rnd.Next(0, 4)];
            ViewBag.two = Two()[rnd.Next(0, 4)];
            ViewBag.three = Three()[rnd.Next(0, 4)];
            ViewBag.four = Four()[rnd.Next(0, 4)];
            ViewBag.five = Five()[rnd.Next(0, 4)];
            ViewBag.six = Six()[rnd.Next(0, 4)];
            ViewBag.seven = Seven()[rnd.Next(0, 4)];
            ViewBag.eight = Eight()[rnd.Next(0, 4)];
            ViewBag.nine = Nine()[rnd.Next(0, 4)];

            return View();
        }

        public List<string> One()
        {
            List<string> one = new List<string>
            {
                "In the dark woods ",
                "In the lonely crocodile ",
                "In the barbaric north ",
                "In the enchanted room ",
            };

            return one;
        }

        public List<string> Two()
        {
            List<string> two = new List<string>
            {
                "you find yourself alone ",
                "you find your friend dead ",
                "you're being chased ",
                "you march onwards ",
            };

            return two;
        }

        public List<string> Three()
        {
            List<string> three = new List<string>
            {
                "then all of a sudden ",
                "out of nowhere comes ",
                "to your surprise, you see ",
                "in the distance you see ",
            };

            return three;
        }

        public List<string> Four()
        {
            List<string> four = new List<string>
            {
                "a giant troll ",
                "your maths teacher ",
                "argus filch ",
                "the nameless scorpion Ted ",
            };

            return four;
        }

        public List<string> Five()
        {
            List<string> five = new List<string>
            {
                "attacks you. ",
                "interrupts you. ",
                "traps you. ",
                "hugs you. "
            };

            return five;
        }

        public List<string> Six()
        {
            List<string> six = new List<string>
            {
                "She's too strong, you can't get away. ",
                "He's right next to you, his breath on your neck. ",
                "She's tough, and your stupid. ",
                "As your powerless, he brings you closer. ",
            };

            return six;
        }

        public List<string> Seven()
        {
            List<string> seven = new List<string>
            {
                "You've been lured, it's all over. ",
                "Your arms cut off, you're left eviscerated. ",
                "This isn't nice. This isn't the cuddle fairy, ",
                "The life drains from you. ",
            };

            return seven;
        }

        public List<string> Eight()
        {
            List<string> eight = new List<string>
            {
                "You meet death ",
                "You're alone, ",
                "You embrace, and soldier through ",
                "You eat a pickle ",
            };

            return eight;
        }

        public List<string> Nine()
        {
            List<string> nine = new List<string>
            {
                "and you're now dead.",
                "and your friends laugh.",
                "and then its all fine.",
                "but it was only a dream.",
            };

            return nine;
        }
    }
}
